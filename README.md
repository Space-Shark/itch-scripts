# Itch.IO Push Scripts

## Setup
1. Create a folder to save your builds (ex: `Builds/`)
1. Download this repository into that folder `Builds/itch-scripts`
1. Run `butler-make.sh` and provide
    1. The company name as it appears in your itch url (company.itch.com)
    1. The game as it appears in your itch url (company.itch.com/gamename)
    1. The game name that you use on your computer


## Example Setups
```
./butler-make.sh mycompany bigfight fighter
# Creates butler-push-fighter.sh which uploads to mycompany.itch.io/bigfighter
```

## Use
1. Create a new folder for each build version (ex: `Builds/1.0`)
1. Name your builds appropriately `[your game]-[channel].zip` and put them in the appropriate version folder
1. Push a certain platform by running `build-[platform].sh [your game] [version]`
1. OR run `build-all.sh [your game] [version]` to push everything up to Itch.io

## Example Folder Structure
```
* Builds
  * itch-scripts
    * butler-\*.sh
  * 1.0
    * fighter-win.zip
    * fighter-android.zip
  * 1.1
    * fighter-win.zip
    * fighter-android.zip
  * 1.1.1a2
    * fighter-win.zip
  * 2.0
    * fighter-android.zip
    * fighter-webgl.zip
```

## Example Push
```
cd itch-scripts
./butler-win.sh fighter 1.0
./butler-android.sh fighter 1.1
```
