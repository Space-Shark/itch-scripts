#/bin/sh

# $1 - company on itch
# $2 - game on itch
# $3 - game on computer

COM=$1
IGAME=$2
CGAME=$3

echo "butler push ../\$1/$CGAME-\$2.zip $COM/$IGAME:\$2 --userversion \$1" > butler-push-$CGAME.sh
